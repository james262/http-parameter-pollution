'use strict';

// requirements
const express = require('express');
const payment = require('./payment');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });


app.get('/', (req, res) => {
    try {
        const { action, amount } = getQuery(req)
        res.send(payment(action, amount))
    } catch (e) {
        res.status(400).send('You can only transfer an amount');
    }
});

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = app;

function getQuery(req) {
    const action = req.query.action
    if (action !== 'transfer') throw new Error('nah')

    if (req.query.amount == null || !(['string', 'number'].includes(typeof req.query.amount))) throw new RangeError('nah please')
    const amount = Number(req.query.amount)
    if (isNaN(amount)) throw new RangeError('not this time')
    return {action, amount}
}
